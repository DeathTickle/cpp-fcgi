#include <iostream>
#include <vector>
#include <string>

#include <cgicc/CgiDefs.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTTPHTMLHeader.h>
#include <cgicc/HTMLClasses.h>

using namespace std;
using namespace cgicc;

int main(int argc, char **argv)
{
	Cgicc cgi;
	const CgiEnvironment& env = cgi.getEnvironment();
	
	cout << HTTPHTMLHeader() << endl;	
	cout << env.getRemoteAddr() << endl;
	cout << "Some text" << endl;

	return 0;
}
